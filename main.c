#include <stdio.h>
#include <stdlib.h>
#include "rpmvercmp.c"

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: %s <version1> <version2>\n", argv[0]);
        return 1;
    }

    const char *version1 = argv[1];
    const char *version2 = argv[2];
    int result = rpmvercmp(version1, version2);
    printf("%d\n", result);
    return 0;
}