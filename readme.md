# Intro
This repo is meant to help test the accuracy of adding rpm support to [semver_dialects](gitlab.com/gitlab-org/ruby/gems/semver_dialects).
It is a copy of the [rpmvercmp](https://github.com/rpm-software-management/rpm/blob/master/rpmio/rpmvercmp.c) code from [rpm](https://github.com/rpm-software-management/rpm/blob/master) where I hardcoded the dependent methods referenced in other files.

# Build
gcc main.c -o rpmvercmp

# Usage
./rpmvercmp v1 v2